package com.twuc.hateoasdemo;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeRepository {
    private List<Employee> employees = new ArrayList<>();

    public EmployeeRepository() {
        employees.add(new Employee("1", "Foo"));
        employees.add(new Employee("2", "Bar"));
    }

    Employee findById(String id) {
        return employees.stream().filter(e -> e.getId().equals(id)).findFirst().get();
    }

    public List<Employee> findAll() {
        return this.employees;
    }

    public Employee save(Employee employee) {
        this.employees.add(employee);
        return employee;
    }
}